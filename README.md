# Users API

API that manages RPG users.

Before starting the server, copy .env.default and rename it to .env !\
Then fill the environment variables inside with your own values.