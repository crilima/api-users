import { Pool } from 'pg';
import config from '../config';

const pool = new Pool({
  user: config.db.user,
  host: config.db.host,
  database: config.db.name,
  password: config.db.password,
  port: config.db.port,
});

const getUsers = () => {
  return pool.query(`SELECT * FROM ${config.db.schema}."User" ORDER BY id ASC`);
};

const getUserById = id => {
  return pool.query(
    `SELECT * FROM ${config.db.schema}."User" WHERE ${config.db.schema}."User".id = $1`,
    [id],
  );
};

const createUser = (id, userName, fullName, mail, phone) => {
  return pool.query(
    `INSERT INTO ${config.db.schema}."User" (id, fullname, username, mail, phone) VALUES ($1, $2, $3, $4, $5) RETURNING id`,
    [id, fullName, userName, mail, phone],
  );
};

const updateUser = (id, fullName, userName, mail, phone) => {
  return pool.query(
    `UPDATE ${config.db.schema}."User" SET fullname = $1, username = $2, mail = $3, phone = $4 WHERE id = $5`,
    [fullName, userName, mail, phone, id],
  );
};

const deleteUser = id => {
  return pool.query(
    `DELETE FROM ${config.db.schema}."User" WHERE ${config.db.schema}."User".id = $1`,
    [id],
  );
};

const follow = (idUser, idTarget) => {
  return pool.query(
    `INSERT INTO ${config.db.schema}."Relationship" ("idCurrentUser", "idTargetUser", "idRelationshipStatus" ) \
    VALUES ($1, $2, \'e3165f83-d56a-4e75-b5e2-80d4d184c079\')`,
    [idUser, idTarget],
  );
};

const unfollow = (idUser, idTarget) => {
  return pool.query(
    `DELETE FROM ${config.db.schema}."Relationship" \
    WHERE ${config.db.schema}."Relationship"."idCurrentUser" = $1 AND \
    ${config.db.schema}."Relationship"."idTargetUser"= $2`,
    [idUser, idTarget],
  );
};

const getAllFollowingUsers = id => {
  return pool.query(
    `SELECT * from ${config.db.schema}."User" u JOIN ${config.db.schema}."Relationship" r ON U.id = r."idCurrentUser" \
    WHERE r."idTargetUser" = $1 AND r."idRelationshipStatus" = \'e3165f83-d56a-4e75-b5e2-80d4d184c079\'`,
    [id],
  );
};

const getAllFollowedUsers = id => {
  return pool.query(
    `SELECT * from ${config.db.schema}."User" u JOIN ${config.db.schema}."Relationship" r ON U.id = r."idTargetUser" \
    WHERE r."idCurrentUser" = $1 AND r."idRelationshipStatus" = \'e3165f83-d56a-4e75-b5e2-80d4d184c079\'`,
    [id],
  );
};

export default {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
  follow,
  unfollow,
  getAllFollowingUsers,
  getAllFollowedUsers,
};
